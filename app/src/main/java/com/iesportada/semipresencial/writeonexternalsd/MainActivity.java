package com.iesportada.semipresencial.writeonexternalsd;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.example.escribirenmemoriaexterna.Memoria;
import com.iesportada.semipresencial.writeonexternalsd.databinding.ActivityMainBinding;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String FICHERO = "ficheroExterna.txt";
    private static final int REQUEST_WRITE = 1;
    private ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.buttonEscribir.setOnClickListener(this);
        binding.buttonLeer.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        // Si pulsamos en el botón escribir
        if (view == binding.buttonEscribir)
            guardar();
            // Si pulsamos en el botón leer
        else if (view == binding.buttonLeer)
            leer();
    }

    public void guardar() {
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        // comprobar los permisos
        if (ActivityCompat.checkSelfPermission(this, permiso) != PackageManager.PERMISSION_GRANTED) {
            // pedir los permisos necesarios, porque no están concedidos
            ActivityCompat.requestPermissions(this, new String[]{permiso}, REQUEST_WRITE);
            // Cuando se cierre el cuadro de diálogo se ejecutará onRequestPermissionsResult
        } else {
            // Permisos ya concedidos
            escribir();
        }
    }

    private void escribir() {

        if(Memoria.disponibleEscritura()){
            try {
                if (Memoria.escribirExterna(FICHERO, binding.editText.getText().toString())){
                    binding.textView.setText(Memoria.mostrarPropiedadesExterna(FICHERO));
                    mostrarMensaje("Fichero escrito con éxito.");
                }
            } catch (IOException e) {
                e.printStackTrace();
                mostrarMensaje("Error en la escritura del fichero." + FICHERO + e.getMessage());
                binding.editText.setText("");
            }
        } else{
            binding.textView.setText("Error al escribir en el fichero." + FICHERO);
            binding.editText.setText("");
        }
    }

    private void leer() {
        if(Memoria.disponibleLectura()){
            try {
                binding.editText.setText(Memoria.leerExterna(FICHERO));
                mostrarMensaje("Fichero leido OK");
            } catch (IOException e) {
                e.printStackTrace();
                mostrarMensaje("Error en lectura" + FICHERO + e.getMessage());
                binding.editText.setText("");
            }
        }else{
            binding.textView.setText("Memoria externa no disponible: " + FICHERO);
            binding.editText.setText("");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        // chequeo los permisos de nuevo
        switch (requestCode) {
            case REQUEST_WRITE:
                if (ActivityCompat.checkSelfPermission(this, permiso) == PackageManager.PERMISSION_GRANTED) {
                    // permiso concedido
                    escribir();
                } else {
                    // no hay permiso
                    mostrarMensaje("No hay permiso para escribir en la memoria externa");
                }
                break;
        }
    }
    private void mostrarMensaje(String texto) {
        Toast.makeText(this, texto, Toast.LENGTH_LONG).show();
    }
}

